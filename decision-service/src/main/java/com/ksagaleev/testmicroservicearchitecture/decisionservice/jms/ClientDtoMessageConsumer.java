package com.ksagaleev.testmicroservicearchitecture.decisionservice.jms;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.ksagaleev.testmicroservicearchitecture.decisionservice.data.model.dto.ClientDto;
import com.ksagaleev.testmicroservicearchitecture.decisionservice.data.model.entity.DecisionDetails;
import com.ksagaleev.testmicroservicearchitecture.decisionservice.service.DecisionDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class ClientDtoMessageConsumer {

    @Autowired
    DecisionDetailsService decisionDetailsService;

    @JmsListener(destination = "${spring.activemq.queue}")
    public void receiveClientDtoMessage(String clientDtoJson) {
        log.info("received JSON from queue = " + clientDtoJson);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.registerModule(new JavaTimeModule());

        ClientDto receivedClientDto = null;
        try {
            receivedClientDto = objectMapper.readValue(clientDtoJson, ClientDto.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(receivedClientDto != null) {
            log.info("deserialized clientDto = " + receivedClientDto.toString());
            decisionDetailsService.makeDecisionForClient(receivedClientDto);
        }
    }
}
