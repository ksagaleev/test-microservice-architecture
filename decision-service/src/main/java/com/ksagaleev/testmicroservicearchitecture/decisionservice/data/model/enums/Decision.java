package com.ksagaleev.testmicroservicearchitecture.decisionservice.data.model.enums;

public enum Decision {
    APPROVED,
    DECLINED
}
