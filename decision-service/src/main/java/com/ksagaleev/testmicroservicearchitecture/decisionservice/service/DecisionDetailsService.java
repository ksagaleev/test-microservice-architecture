package com.ksagaleev.testmicroservicearchitecture.decisionservice.service;

import com.ksagaleev.testmicroservicearchitecture.decisionservice.data.model.dto.ClientDto;
import com.ksagaleev.testmicroservicearchitecture.decisionservice.data.model.entity.DecisionDetails;

import java.util.List;

public interface DecisionDetailsService {
    List<DecisionDetails> getAllDecisionDetails();
    DecisionDetails addDecisionDetails(DecisionDetails decisionDetails);
    DecisionDetails getDecisionByClientId(Long clientId);
    DecisionDetails makeDecisionForClient(ClientDto clientDto);
}
