package com.ksagaleev.testmicroservicearchitecture.decisionservice.data.model.entity;

import com.ksagaleev.testmicroservicearchitecture.decisionservice.data.model.enums.Decision;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;

@Data
public class DecisionDetails {

    @Id
    private ObjectId id;
    private LocalDate date;
    private Long cliendId;
    private Decision decision;

    public DecisionDetails() {}

    public DecisionDetails(LocalDate date, Long cliendId, Decision decision) {
        this.date = date;
        this.cliendId = cliendId;
        this.decision = decision;
    }
}
