package com.ksagaleev.testmicroservicearchitecture.decisionservice.service;

import com.ksagaleev.testmicroservicearchitecture.decisionservice.data.model.dto.ClientDto;
import com.ksagaleev.testmicroservicearchitecture.decisionservice.data.model.entity.DecisionDetails;
import com.ksagaleev.testmicroservicearchitecture.decisionservice.data.model.enums.Decision;
import com.ksagaleev.testmicroservicearchitecture.decisionservice.data.repo.DecisionDetailsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class DecisionDetailsServiceImpl implements DecisionDetailsService {

    @Autowired
    DecisionDetailsRepo decisionDetailsRepo;

    @Override
    public List<DecisionDetails> getAllDecisionDetails() {
        return decisionDetailsRepo.findAll();
    }

    @Override
    public DecisionDetails addDecisionDetails(DecisionDetails decisionDetails) {
        return decisionDetailsRepo.save(decisionDetails);
    }

    @Override
    public DecisionDetails getDecisionByClientId(Long clientId) {
        return decisionDetailsRepo.findByCliendId(clientId);
    }

    @Override
    public DecisionDetails makeDecisionForClient(ClientDto clientDto) {
        DecisionDetails existingDecisionDetails = decisionDetailsRepo.findByCliendId(clientDto.getId());

        DecisionDetails decisionDetailsToSave;
        Decision decision = isClientOk(clientDto) ? Decision.APPROVED : Decision.DECLINED;

        if (existingDecisionDetails == null) {
            decisionDetailsToSave = new DecisionDetails(LocalDate.now(), clientDto.getId(), decision);
        } else {
            decisionDetailsToSave = existingDecisionDetails;
            decisionDetailsToSave.setDate(LocalDate.now());
            decisionDetailsToSave.setDecision(decision);
        }

        return decisionDetailsRepo.save(decisionDetailsToSave);
    }

    private boolean isClientOk(ClientDto clientDto) {
        byte minYears = 18;
        byte minExperience = 2;
        int minSalary = 40000;

        int clientAge = LocalDate.now().getYear() - clientDto.getBirthday().getYear();

        if (clientAge > minYears
                && clientDto.getWorkExperience() > minExperience
                && clientDto.getSalary() > minSalary) {
            return true;
        } else {
            return false;
        }
    }
}
