package com.ksagaleev.testmicroservicearchitecture.decisionservice.data.repo;

import com.ksagaleev.testmicroservicearchitecture.decisionservice.data.model.entity.DecisionDetails;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DecisionDetailsRepo extends MongoRepository<DecisionDetails, String> {

    public DecisionDetails findByCliendId(Long clientId);
}
