package com.ksagaleev.testmicroservicearchitecture.decisionservice.controller;

import com.ksagaleev.testmicroservicearchitecture.decisionservice.data.model.entity.DecisionDetails;
import com.ksagaleev.testmicroservicearchitecture.decisionservice.service.DecisionDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/decision-service/decisions")
@Slf4j
public class DecisionController {

    @Autowired
    private DecisionDetailsService decisionDetailsService;

    @GetMapping
    public ResponseEntity<List<DecisionDetails>> getAllDecisionsDetails(){
        List<DecisionDetails> decisions = decisionDetailsService.getAllDecisionDetails();
        HttpStatus status = decisions == null ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return new ResponseEntity<>(decisions, status);
    }

    @PostMapping
    public ResponseEntity<DecisionDetails> addDecisionDetails(@RequestBody DecisionDetails decisionToAdd) {
        log.info(decisionToAdd.toString());
        DecisionDetails addedDecision = decisionDetailsService.addDecisionDetails(decisionToAdd);
        HttpStatus status = addedDecision == null ? HttpStatus.BAD_REQUEST : HttpStatus.OK;
        return new ResponseEntity<>(addedDecision, status);
    }

    @GetMapping(value = "/client/{clientId}")
    public ResponseEntity<DecisionDetails> getDecisionForClient(@PathVariable Long clientId) {
        log.info("trying to get decision for clientId = " + clientId);
        DecisionDetails decisionDetails = decisionDetailsService.getDecisionByClientId(clientId);
        HttpStatus status = decisionDetails == null ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return new ResponseEntity<>(decisionDetails, status);
    }
}
