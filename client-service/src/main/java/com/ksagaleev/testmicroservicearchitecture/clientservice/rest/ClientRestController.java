package com.ksagaleev.testmicroservicearchitecture.clientservice.rest;

import com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.entity.Client;
import com.ksagaleev.testmicroservicearchitecture.clientservice.service.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping(value = "/client-service/clients")
@Slf4j
public class ClientRestController {

    @Autowired
    public ClientService clientService;

    @Autowired
    public JobLauncher jobLauncher;

    @Autowired
    public Job job;

    @GetMapping
    public ResponseEntity<List<Client>> getAllClients() {
        List<Client> clients = clientService.getAllClients();
        HttpStatus status = clients == null ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return new ResponseEntity<>(clients, status);
    }

    @PostMapping
    public ResponseEntity<Client> addClient(@RequestBody Client clientToAdd) {
        Client addedClient = clientService.addClient(clientToAdd);
        HttpStatus status = addedClient == null ? HttpStatus.BAD_REQUEST : HttpStatus.OK;
        return new ResponseEntity<>(addedClient, status);
    }

    @GetMapping(value = "/{id}/applyForCredit")
    public ResponseEntity<String> makeDecisionForClient(@PathVariable("id") Long clientId) {
        clientService.getDecisionForClient(clientId);
        return new ResponseEntity<>("Your application is on review", HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/getDecision")
    public ResponseEntity<String> getDecisionForClient(@PathVariable("id") Long clientId) {
        String decisionServiceUrl = "http://localhost:8081/decision-service";
        String endpoint = "/decisions/client/" + clientId;

        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.exchange(decisionServiceUrl + endpoint,
                    HttpMethod.GET,
                    null,
                    String.class);
            return new ResponseEntity<>(response.getBody(), response.getStatusCode());
        } catch (final HttpClientErrorException httpClientErrorException) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/loadClients")
    public ResponseEntity<List<Client>> loadClientsFromCsv() {
        log.info("trying to load clients from csv file");

        try {
            JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                    .toJobParameters();
            jobLauncher.run(job, jobParameters);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("Job failed");
        }

        List<Client> clients = clientService.getAllClients();
        HttpStatus status = clients == null ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return new ResponseEntity<>(clients, status);
    }
}
