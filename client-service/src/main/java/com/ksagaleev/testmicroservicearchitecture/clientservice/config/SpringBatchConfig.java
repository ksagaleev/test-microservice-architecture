package com.ksagaleev.testmicroservicearchitecture.clientservice.config;

import com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.entity.Client;
import com.ksagaleev.testmicroservicearchitecture.clientservice.data.repo.ClientRepo;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.data.builder.RepositoryItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
@EnableBatchProcessing
public class SpringBatchConfig {

    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    @Autowired
    private ClientRepo clientRepo;

    @Bean
    public FlatFileItemReader<Client> reader() {
        return new FlatFileItemReaderBuilder<Client>()
                .name("clientItemReader")
                .resource(new ClassPathResource("clients.csv"))
                .delimited()
                .names(new String[]{"firstName", "middleName", "lastName", "sex", "birthday", "company", "workExperience", "salary"})
                .fieldSetMapper(new CustomBeanWrapperFieldSetMapper<Client>() {{
                    setTargetType(Client.class);
                }})
                .build();
    }

    @Bean
    public RepositoryItemWriter<Client> writer() {
        return new RepositoryItemWriterBuilder<Client>()
                .methodName("save")
                .repository(clientRepo)
                .build();
    }


    @Bean
    public Job importClientsJob(Step step1) {
        return jobs.get("importClientsJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1() {
        return steps.get("step1")
                .<Client, Client> chunk(5)
                .reader(reader())
                .writer(writer())
                .build();
    }
}
