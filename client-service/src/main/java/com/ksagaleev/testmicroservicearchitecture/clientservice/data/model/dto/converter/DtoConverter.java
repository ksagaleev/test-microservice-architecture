package com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.dto.converter;

public interface DtoConverter<D,E> {
    D convertToDTO(E entity);
    E convertToEntity(D dto);
}
