package com.ksagaleev.testmicroservicearchitecture.clientservice.service;

import com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.dto.ClientDto;
import com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.dto.converter.ClientDtoConverter;
import com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.entity.Client;
import com.ksagaleev.testmicroservicearchitecture.clientservice.data.repo.ClientRepo;
import com.ksagaleev.testmicroservicearchitecture.clientservice.jms.ClientDtoMessageProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    private ClientRepo clientRepo;
    private ClientDtoMessageProducer clientDtoMessageProducer;
    private ClientDtoConverter clientDtoConverter;

    public ClientServiceImpl(@Autowired ClientRepo clientRepo,
                             @Autowired ClientDtoMessageProducer clientDtoMessageProducer,
                             @Autowired ClientDtoConverter clientDtoConverter) {
        this.clientRepo = clientRepo;
        this.clientDtoMessageProducer = clientDtoMessageProducer;
        this.clientDtoConverter = clientDtoConverter;
    }

    public List<Client> getAllClients() {
        return clientRepo.findAll();
    }

    public Client addClient(Client client) {
        return clientRepo.save(client);
    }

    @Override
    public void getDecisionForClient(Long clientId) {
        Client client = clientRepo.getOne(clientId);
        ClientDto clientDtoToSend = clientDtoConverter.convertToDTO(client);
        clientDtoMessageProducer.sendClientDtoMessage(clientDtoToSend);
    }
}
