package com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.enums;

public enum Sex {
    MALE,
    FEMALE
}
