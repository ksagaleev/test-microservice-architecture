package com.ksagaleev.testmicroservicearchitecture.clientservice.service;

import com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.entity.Client;

import java.util.List;

public interface ClientService {
    List<Client> getAllClients();
    Client addClient(Client client);
    void getDecisionForClient(Long clientId);
}
