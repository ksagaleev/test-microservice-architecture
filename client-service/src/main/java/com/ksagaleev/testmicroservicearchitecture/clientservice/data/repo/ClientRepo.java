package com.ksagaleev.testmicroservicearchitecture.clientservice.data.repo;

import com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepo extends JpaRepository<Client, Long> {
}
