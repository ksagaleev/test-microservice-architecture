package com.ksagaleev.testmicroservicearchitecture.clientservice.jms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.dto.ClientDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ClientDtoMessageProducer {

    @Value("${spring.activemq.queue}")
    private String destinationQueue;

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendClientDtoMessage(ClientDto clientDto) {
        log.info("sending clientDto='{}' to destination='{}'", clientDto, destinationQueue);
        String clientDtoJson = convertClientDtoToJson(clientDto);
        jmsTemplate.convertAndSend(destinationQueue, clientDtoJson);
    }

    private String convertClientDtoToJson(ClientDto clientDto) {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.registerModule(new JavaTimeModule());

        try {
            return objectMapper.writeValueAsString(clientDto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
