package com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.dto.converter;

import com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.dto.ClientDto;
import com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.entity.Client;
import org.springframework.stereotype.Component;

@Component
public class ClientDtoConverter implements DtoConverter<ClientDto, Client> {

    @Override
    public ClientDto convertToDTO(Client entity) {
        return ClientDto.builder()
                .id(entity.getId())
                .birthday(entity.getBirthday())
                .salary(entity.getSalary())
                .workExperience(entity.getWorkExperience())
                .build();
    }

    @Override
    public Client convertToEntity(ClientDto dto) {
        return Client.builder()
                .birthday(dto.getBirthday())
                .build(); //TODO some nice converting
    }
}
