package com.ksagaleev.testmicroservicearchitecture.clientservice.data.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientDto implements Serializable {

    private Long id;
    private LocalDate birthday;
    private Byte workExperience;
    private Integer salary;
}
